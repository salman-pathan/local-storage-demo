$(document).ready(() => {

    const lsKeyEmail = "keyEmail"
    const lsKeyPassword = "keyPassword"

    //  Retrieving values from local storage
    var ls = new Storage()
    var lsValEmail = ls.get(lsKeyEmail)
    var lsValPassword = ls.get(lsKeyPassword)

    if (lsValEmail != null) {
        $('#inputEmail').val(lsValEmail)
    }

    if (lsValPassword != null) {
        $('#inputPassword').val(lsValPassword)
    }

    $('#btnSubmit').click(() => {
        var inputEmail = $('#inputEmail').val()
        var inputPassword = $('#inputPassword').val()

        //  Add input validation. Expecting sunny day scenario.

        //  Adding values to local storage
        ls.set(lsKeyEmail, inputEmail)
        ls.set(lsKeyPassword, inputPassword)

    })

    

})