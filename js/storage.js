function Storage() {

    this.get = (key) => {
        if (key != null) {
            v = localStorage.getItem(key)
            console.log(key, v)
            return v
        }
    }

    this.set = (key, value) => {
        if (key != null) {
            localStorage.setItem(key, value)
        }
    }

}